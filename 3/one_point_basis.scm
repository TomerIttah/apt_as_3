;; Church numerals
(define c0
  (lambda (s)
    (lambda (z)
      z)))

(define s+
  (lambda (n)
    (lambda (s)
      (lambda (z)
	(s ((n s) z))))))

(define c+
  (lambda (ca)
    (lambda (cb)
      ((ca s+) cb))))

(define c*
  (lambda (ca)
    (lambda (cb)
      ((cb (c+ ca)) c0))))

(define nat->church
  (lambda (n)
    (if (zero? n)
	c0
	(s+ (nat->church (- n 1))))))

(define church->nat
  (lambda (cn)
    ((cn (lambda (n) (+ n 1)))
     0)))

(define pi_12 (lambda (p) (p (lambda (a) (lambda (b) a)))))
(define pi_22 (lambda (p) (p (lambda (a) (lambda (b) b)))))
(define f (lambda (p) (lambda (x) ((x (s+ (pi_12 p))) ((c* (pi_12 p)) (pi_22 p))))))
(define factorial (lambda (n) (pi_22 ((n f) (lambda (x) ((x (s+ c0)) (s+ c0)))))))
(define fpredecessor (lambda (p) (lambda (x) ((x (pi_22 p)) (s+ (pi_22 p))))))
(define predecessor (lambda (n) (pi_12 ((n fpredecessor) (lambda (x) ((x c0) c0))))))
(define church_monus (lambda (a) (lambda (b) ((b predecessor) a))))
(define church_true (lambda (x) (lambda (y) x)))
(define church_false (lambda (x) (lambda (y) y)))
(define church_zero? (lambda (n) ((n (lambda (x) church_false)) church_true)))
(define church_and (lambda (a) (lambda (b) ((a b) church_false))))
(define church_ge (lambda (a) (lambda (b) (church_zero? ((church_monus b) a)))))
(define church_le (lambda (a) (lambda (b) (church_zero? ((church_monus a) b)))))
(define church_= (lambda (a) (lambda (b) ((church_and ((church_ge a) b)) ((church_le a) b)))))

(define S
  (lambda (x)
    (lambda (y)
      (lambda (z)
        ((x z) (y z))))))

(define K (lambda (x) (lambda (y) x)))

(define Ycurry
  (lambda (f)
    ((lambda (x)
       (f (lambda args
            (apply (x x) args))))
     (lambda (x)
       (f (lambda args
            (apply (x x) args)))))))

; Data structure for the elements
(define ds
  (Ycurry
   (lambda (m)
     (lambda (p)
       ((p 1)
        (lambda (p1)
          ((p1 S)
           (lambda (p2)
             ((p2 K)
              (lambda (p3)
                ((p3 nat->church)
                 (lambda (p4)
                   ((p4 church->nat) m))))))))))
       )))

(define X
  (lambda (x)
    ((x (lambda (m)
          (lambda (b)
            (lambda (a)
              (if (number? (pi_12 a))
                  (lambda (x)
                    ((x m) (pi_22 b))) (pi_12 b)))))) ds)))

((X X) X) ; 1 ("base marker")
((X X) (X X)) ; #<procedure:S>
((X X) (X (X X))) ; #<procedure:K>
((X X) (X (X (X X)))) ; #<procedure:nat->church>
((X X) (X (X (X (X X))))) ; #<procedure:church->nat>

;; Takes a natural number in Scheme, and returns its factorial, as a natural number in Scheme,
;; while the actual computation of factorial is carried out using Church numerals.
(define factX
  (lambda (n)
    (((X X) (X (X (X (X X)))))
     (((((X X) (X X)) ((((X X) (X X)) ((((X X) (X X)) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))) (((X X) (X (X X))) ((((X X) (X X)) ((((X X) (X X)) (((X X) (X (X X))) ((((X X) (X X)) (((X X) (X (X X))) ((((X X) (X X)) ((((X X) (X X)) (((X X) (X (X X))) ((X X) (X X)))) ((((X X) (X X)) (((X X) (X (X X))) ((X X) (X (X X))))) ((X X) (X X))))) (((X X) (X (X X))) ((X X) (X (X X))))))) ((((X X) (X X)) (((X X) (X (X X))) (((X X) (X X)) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))))) ((X X) (X (X X))))))) ((((X X) (X X)) (((X X) (X (X X))) (((X X) (X X)) ((((X X) (X X)) (((X X) (X (X X))) ((X X) (X X)))) ((X X) (X (X X))))))) ((((X X) (X X)) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))) (((X X) (X (X X))) ((X X) (X (X X)))))))) ((((X X) (X X)) ((((X X) (X X)) (((X X) (X (X X))) ((((X X) (X X)) (((X X) (X (X X))) ((X X) (X X)))) ((X X) (X (X X)))))) ((((X X) (X X)) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))) (((X X) (X (X X))) ((X X) (X (X X))))))) ((((X X) (X X)) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))) (((X X) (X (X X))) (((X X) (X (X X))) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X)))))))))))) (((X X) (X (X X))) ((((X X) (X X)) ((((X X) (X X)) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))) (((X X) (X (X X))) ((((X X) (X X)) ((((X X) (X X)) (((X X) (X (X X))) ((X X) (X X)))) ((X X) (X (X X))))) (((X X) (X (X X))) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))))))) (((X X) (X (X X))) ((((X X) (X X)) ((((X X) (X X)) (((X X) (X (X X))) ((X X) (X X)))) ((X X) (X (X X))))) (((X X) (X (X X))) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X))))))))))) (((X X) (X (X X))) (((X X) (X (X X))) ((((X X) (X X)) ((X X) (X (X X)))) ((X X) (X (X X)))))))
      (((X X) (X (X (X X)))) n)))
    )
    )

; Tests
(and
 (= (factX 0) 1)
 (= (factX 1) 1)
 (= (factX 2) 2)
 (= (factX 3) 6)
 (= (factX 4) 24)
 (= (factX 5) 120)
 (= (factX 6) 720)
)
