;; 1. The expression
(let ((f (call/cc (lambda (k) k))))
  (new-cafe)
  (f f))

;; 2. Separate to context- >(let ((f O)) (new-cafe) (f f)) & expression- (call/cc (lambda (k) k))
;;>(let ((f (call/cc (lambda (k) k))))
;;  (new-cafe)
;;  (f f))

;; 3. Equivalence
;;>(let ((f ((lambda (k) k) (lambda (v) >(let ((f v)) (new-cafe) (f f))))))
;;  (new-cafe)
;;  (f f))

;; 4. Eval
;;>(let ((f (lambda (v) >(let ((f v)) (new-cafe) (f f)))))
;;  (new-cafe)
;;  (f f))

;; 5. new cafe prompt
;;>>

;; 5. Let body (executed upon leaving the cafe with (exit) or <ctrl-D> or by paying the bill)
;;> (f f)

;; 6. Let body return eval
;;> ((lambda (v) >(let ((f v)) (new-cafe) (f f)))
;;    (lambda (v) >(let ((f v)) (new-cafe) (f f))))

;; 7. Eval (going back to step 4)
;;> >(let ((f (lambda (v) >(let ((f v)) (new-cafe) (f f)))))
;;   (new-cafe)
;;   (f f))
