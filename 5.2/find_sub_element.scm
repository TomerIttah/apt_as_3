;; Signature: find(pred, sexpr)
;; Type: (Sexpr->boolean)*Sexpr->Sexpr
;; Purpose: Takes a predicate and an S-expression, and returns some sub-expression that satisfies the predicate,
;;          or #f if no such sub-expression exists.
;; Pre-conditions:
;;   1. It should support everything but vectors.
;;   2. Side effects are not allowed.
;;   3. It should be written in direct style, not CPS.
;;   4. It should terminates as soon as it finds an element that satisfies the predicate.
;; Assumption:
;;   1. s-expression is considered to be a sub expression for him self (e.g. (find (lambda (e) (number? e)) 5) -> 5
;;   2. s-expression given as argument could be nested (e.g. '((1 2) 2))
;; Sample Tests:
;; > (find (lambda (e) (and (number? e) (integer? e) (even? e))) ’())
;; #f
;; > (find (lambda (e) (and (number? e) (integer? e) (even? e))) ’(moshe))
;; #f
;; > (find (lambda (e) (and (number? e) (integer? e) (even? e))) ’(moshe 1 2 3 4))
;; 2
;; > (find (lambda (e) (and (number? e) (integer? e) (even? e))) ’(moshe 1 2111 3 4))
;; 4
(define nfind
  (lambda (pred sexpr k)
    (cond
      ((pred sexpr) (k sexpr)) ; Exits immidietly as it found an element that satisfies the predicate
      ((pair? sexpr)
       (or (nfind pred (car sexpr) k) (nfind pred (cdr sexpr) k)))
      (else #f))
    )
  )

(define find
  (lambda (pred sexpr)
    (call/cc
     (lambda (k)
       (nfind pred sexpr k)
     ))
  )
)

;; Tests
 (and
  (equal? (find (lambda (e) (boolean? e)) '(#t)) #t)
  (equal? (find (lambda (e) (and (number? e) (integer? e) (even? e))) '()) #f)
  (equal? (find (lambda (e) (and (number? e) (integer? e) (even? e))) '(moshe)) #f)
  (= (find (lambda (e) (and (number? e) (integer? e) (odd? e))) '(2 ((((1)) (3) #f 2)))) 1)
  (= (find (lambda (e) (and (number? e) (integer? e) (even? e))) '(moshe 1 2 3 4)) 2)
  (= (find (lambda (e) (and (number? e) (integer? e) (even? e))) '(moshe 1 2111 3 4)) 4)
  (= (find (lambda (e) (and (number? e) (integer? e) (even? e))) '(moshe (1 3 5 (2 1)) 2111 3 1)) 2)
  (= (find (lambda (e) (and (number? e) (integer? e) (even? e))) '(moshe (1 3 5 (3 1)) 2111 66 1)) 66)
  (equal? (find (lambda (e) (and (number? e) (integer? e) (even? e))) '(moshe (1 3 5 (3 1)) 2111 3 1 (1 1 1) 1)) #f))

;; 1. Separate the context- >(define find (lambda (pred sexpr) O)) & expression- (call/cc (lambda (k) (nfind pred sexpr k)))
;;>(define find
;;  (lambda (pred sexpr)
;;    (call/cc
;;     (lambda (k)
;;       (nfind pred sexpr k)
;;     ))
;;  )
;;)

;; 2. Equivalence
;;>(define find
;;  (lambda (pred sexpr)
;;    ((lambda (k) (nfind pred sexpr k))
;;     (lambda (v) >(define find (lambda (pred sexpr) v))))
;;  )
;;)

;; 3. Eval
;;>(define find
;;  (lambda (pred sexpr)
;;    (nfind pred sexpr (lambda (v) >(define find (lambda (pred sexpr) v))))
;;     )
;;  )

;; 4. Finding an expression that satisfies the predicate in nfind and applying (k e)
;;((lambda (v) >(define find (lambda (pred sexpr) v))) e)

;; 5. Eval
;;>(define find (lambda (pred sexpr) e))

;; 6. Lambda body returns e
;;>e
